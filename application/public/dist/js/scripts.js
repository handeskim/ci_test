$(document).ready(function () {
    'use strict';

    const csrfToken = $("meta[name='csrf_token']").attr('content');

    /*== Validate form ==*/
    function validate(formSelector) {
        let errors = 0

        $(formSelector).find("[data-rule-required='true']").each(function () {
            const _this = $(this);
            const errorLabelClass = 'invalid-feedback';
            const errorInputClass = 'is-invalid';

            let value = $.trim(_this.val());
            let errorSelector = _this.siblings(`label.${errorLabelClass}`);

            if (value === '') {
                let msg = _this.data('msg-required');
                let errorHtml = `<label class="${errorLabelClass}">${msg}</label>`;

                if (errorSelector.length === 0) {
                    _this.after(errorHtml);
                }
                errorSelector.show();
                _this.addClass(errorInputClass);
                errors++;
                return;
            }

            _this.removeClass('is-invalid');
            errorSelector.hide();
        });

        return errors === 0;
    }

    /*== Get list users ==*/
    function getListUsers(data) {
        let html = '';

        data.map(value => {
            html += `
                   <tr>
                        <td>${value.fullname}</td>
                        <td>${moment(value.date_of_birth).format('DD/MM/YYYY')}</td>
                        <td>${value.gender}</td>
                        <td>${value.balancer}</td>
                        <td>${value.created_at}</td>
                        <td class="text-right">
                            <button type="button" class="btn btn-sm btn-danger btn-delete" data-id="${value._id.$id}">
                                <i class="far fa-trash-alt"></i>
                            </button>
                        </td>
                   </tr>
              `;
        });

        $("#myTable tbody").html(html);
    }

    /*== Prepend user ==*/
    function prependUser(data) {
        let html = `
           <tr>
                <td>${data.fullname}</td>
                <td>${moment(data.date_of_birth).format('DD/MM/YYYY')}</td>
                <td>${data.gender}</td>
                <td>${data.balancer}</td>
                <td>${data.created_at}</td>
                <td class="text-right">
                    <button type="button" class="btn btn-sm btn-danger btn-delete" data-id="${data._id.$id}">
                        <i class="far fa-trash-alt"></i>
                    </button>
                </td>
           </tr>
        `;

        $("#myTable tbody").prepend(html);
    }

    /*== Button loader function ==*/
    function buttonLoader(buttonSelector, flag = true) {
        if (buttonSelector.find("i[class^='fa']").length > 0) {
            buttonSelector.find("i[class^='fa']").hide();
        }

        const loaderClass = 'fas fa-spin fa-spinner';

        let loaderHtml = `<i class="${loaderClass} mr-1"></i>`
        if (flag === true) {
            buttonSelector.prepend(loaderHtml);
        } else {
            buttonSelector.find("i[class^='fa']").show();
            buttonSelector.find(`i[class*="${loaderClass}"]`).remove();
        }
        buttonSelector.prop('disabled', flag);
    }

    /*== Ajax function ==*/
    function doAjax(action, method = 'POST', data = {}, callback = null) {
        $.ajax({
            url: action,
            method: method,
            data: data,
            dataType: 'JSON',
            beforeSend: function () {
                buttonLoader($('button:submit'));
            }
        }).done(function (result) {
            Notify.success({title: result.message});
            if (callback === null) {
                return window.location.reload();
            }

            if (typeof callback === 'function') {
                return callback(result);
            }
        }).fail(function (error) {
            console.log(error);
            Notify.error({
                title: 'Error ' + error.status,
                text: error.responseJson?.message || error.statusText
            });
        }).always(function () {
            buttonLoader($('button:submit'), false);
        });
    }

    /*== Input validate event ==*/
    $(document).on('input', "form input[data-rule-required='true']", function () {
        $(this).removeClass('is-invalid')
            .siblings('label.invalid-feedback').hide();
    });

    /*== Form submit event ==*/
    $("form").submit(function (e) {
        e.preventDefault();
        const _this = $(this);

        if (validate(_this)) {
            let action, method, data;
            action = $(this).attr('action');
            method = $(this).attr('method');
            data = $(this).serialize();

            return doAjax(action, method, data, function (result) {
                $("#create-modal").modal('hide');
                prependUser(result.data);
                _this[0].reset();
            });
        }

        return false;
    });

    /*== Delete document ==*/
    $(document).on('click', '.btn-delete', function () {
        const _this = $(this);

        return Notify.delete(function () {
            let _id = _this.data('id');

            return doAjax('base/api', 'DELETE', {_id: _id}, function () {
                _this.parents('tr').remove();
            });
        });
    });

    /*== Search users ==*/
    $(document).on('input', '#search', function () {
        let value = $.trim($(this).val());

        $.ajax({
            url: 'base/api',
            type: 'GET',
            data: {keyword: value},
            dataType: 'json'
        }).done(function (result) {
            getListUsers(result.data);
        }).fail(function (error) {
            console.log(error);
        });
    });
});
