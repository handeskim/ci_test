<!doctype html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta
        name="<?php echo $this->security->get_csrf_token_name(); ?>"
        content="<?php echo $this->security->get_csrf_hash(); ?>"
    />
    <title>{title}</title>
    <base href="<?= base_url() ?>" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="public/dist/css/style.css" />
</head>
<body>

<div class="container">
    <?= $this->load->view($content) ?>
</div>

<script
    src="https://code.jquery.com/jquery-3.5.0.min.js"
    integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ="
    crossorigin="anonymous"></script>
<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.js"></script>
<script type="text/javascript" src="public/dist/js/notify.js"></script>
<script type="text/javascript" src="public/dist/js/scripts.js"></script>
<script type="text/javascript">
    $(document).ready( function () {
        $.fn.dataTable.ext.errMode = "throw";

        $('#myTable').DataTable({
            ordering: true,
            autoWidth: true,
            scrollCollapse: true,
            scrollY: true,
            processing: true,
            destroy: true,
            serverSide: true,
            searching: false,
            ajax: {
                url: 'base/api',
                dataType: 'json',
                type: 'GET',
            },
            order: [0, 'desc'],
            buttons: [],
            columns: [
                {data: 'fullname'},
                {data: 'date_of_birth', render: function (data) {
                    return moment(data).format('DD/MM/YYYY')
                }},
                {data: 'gender'},
                {data: 'balancer'},
                {data: 'created_at', render: function (data) {
                    return moment(data * 1000).format('DD/MM/YYYY');
                }},
                {render: function (data, type, row) {
                    return `
                        <button type="button" class="btn btn-danger btn-sm btn-delete text-right" data-id="${row._id.$id}">
                            <i class="far fa-trash-alt"></i>
                        </button>
                    `;
                }}
            ],
            columnDefs: [
                { className: "text-right", targets: [-1] }
            ]
        });
    });
</script>
</body>
</html>
