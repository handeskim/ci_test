<div class="modal fade" tabindex="-1" role="dialog" id="create-modal">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create user</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="base/api" method="POST">
                    <input type="hidden" name="<?= $this->security->get_csrf_token_name() ?>" value="<?= $this->security->get_csrf_hash() ?>" />
                    <div class="form-group row">
                        <label for="fullname" class="required col-form-label col-md-2">
                            Fullname
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="fullname" id="fullname" class="form-control" data-rule-required="true" data-msg-required="This field is required." />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="date_of_birth" class="required col-form-label col-md-2">
                            Date of birth
                        </label>
                        <div class="col-md-10">
                            <input type="date" name="date_of_birth" id="date_of_birth" class="form-control" data-rule-required="true" data-msg-required="This field is required." />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-2">
                            Gender
                        </label>
                        <div class="col-md-10">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" id="gender-male" value="Male" checked>
                                <label class="form-check-label" for="gender-male">Male</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" id="gender-female" value="Female">
                                <label class="form-check-label" for="gender-female">Female</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="required col-form-label col-md-2" for="balancer">
                            Balancer
                        </label>
                        <div class="col-md-10">
                            <input type="number" min="10000" name="balancer" id="balancer" class="form-control" data-rule-required="true" data-msg-required="This field is required." />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-10 offset-md-2">
                            <button type="submit" class="btn btn-info btn-lg">
                                <i class="fas fa-save mr-1"></i>
                                Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
