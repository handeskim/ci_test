<h1 style="margin-top: 50px;">User list</h1>
<div class="mt-5">
    <div class="row">
        <div class="col-md-7">
            <a href="#create-modal" class="btn btn-primary" data-toggle="modal">
                <i class="fas fa-plus mr-1"></i>
                Create user
            </a>
        </div>
        <div class="col-md-5">
            <input type="text" name="keyword" placeholder="Search..." class="form-control" id="search" />
        </div>
    </div>
</div>
<div class="table-responsive mt-3 mb-5">
    <table class="table table-hover" id="myTable">
        <thead>
            <tr>
                <th>Fullname</th>
                <th>Date of birth</th>
                <th>Genger</th>
                <th>Balancer</th>
                <th>Created date</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>

<?= $this->load->view('create') ?>
