<?php

class Users extends CI_Model
{

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        
        
    }

    public function api_get($id = null)
    {
        if (empty($id)) {
            return $this->mongo_db->order_by(['created_at' => 'desc'])->get('users');
        }

        return $this->mongo_db->where('_id', $id)->find_one('users');
    }

    public function api_post($data)
    {
        if (empty($data)) {
            return false;
        }

        $query = $this->mongo_db->insert('users', $data);
        return $query;
    }

    public function api_delete($id)
    {
        if (empty($id)) {
            return false;
        }

        return $this->mongo_db->where('_id', new \MongoId($id))->delete('users');
    }

    public function api_search($keyword = null)
    {
       
       /*
        bạn search thế này thì chết tôi rồi. 
        quy định fillter là gì ạ. nếu tôi cần search theo tuổi thế nào.
        tôi search theo ngày thế nà?
        
        hãy sửa dụng where($array_field)
        
        if (!empty($keyword)) {
            $this->mongo_db->like('fullname', $keyword);
        }
       
        return $this->mongo_db->get('users');
        */
    }
}
