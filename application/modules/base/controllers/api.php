<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Api extends REST_Controller {
	function __construct()
    {
		parent::__construct();
        $this->users = $this->load->model('users');
	}

    /**
     * List the user
     */
	public function index_get ()
    {
        if (!empty($this->get('keyword'))) {
            $data = $this->users->api_search($this->get('keyword'));
        } else {
            $data = $this->users->api_get();
        }

		return $this->responseApi('List success', $data);
	}

    /**
     * Create user
     */
	public function index_post ()
    {
        sleep(1);
        $data = $this->post();
        $data['created_at'] = strtotime(date('Y-m-d H:i:s'));

        $result = $this->users->api_post($data);
        if (empty($result)) {
            return $this->responseServerError();
        }

        return $this->responseApi(
            'Create success',
            $this->users->api_get($result)
        );
	}

    /**
     * Delete user
     */
	public function index_delete()
    {
        sleep(1);
        $id = $this->delete('_id');

        $result = $this->users->api_delete($id);
        if (empty($result)) {
            return $this->responseServerError();
        }

        return $this->responseApi('Delete success', $result);
	}

    /**
     * Response API message
     *
     * @param string $message
     * @param int $code
     * @param null $data
     */
	protected function responseApi($message = '', $data = null, $code = 200)
    {
        $response = [
            'message' => $message,
            'data' => $data
        ];

        return $this->response($response, $code);
    }

    /**
     * Response server error
     */
    protected function responseServerError()
    {
        return $this->responseApi('Internal server error', null, 500);
    }
}

?>
