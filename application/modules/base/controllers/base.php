<?php
class Base extends MY_Controller{
    protected $data;

	function __construct()
    {
		parent::__construct();
		$this->load->model('users');
        $this->load->helper('url');
		$this->data = [];
	}

    /**
     * Load main layout
     *
     * @param null $data
     * @return mixed
     */
	protected function loadMain($data = null)
    {
        return $this->parser->parse('main', $data);
    }

    /**
     * Get list the user
     * @return mixed
     */
	public function index()
    {
        $this->data['title'] = 'User list';
        $this->data['content'] = 'base';
		return $this->loadMain($this->data);
	}
}
?>
